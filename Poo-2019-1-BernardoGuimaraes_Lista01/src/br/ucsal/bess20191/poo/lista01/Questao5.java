package br.ucsal.bess20191.poo.lista01;

import java.util.Scanner;

public class Questao5 {
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		int num1;
		double fat;
		double invfat;
		System.out.println("informe um valor");
		num1 = sc.nextInt();
		fat = calcularFatorial(num1);
		invfat = calcularInversoFatorial(num1, fat);

		impressao(invfat);

	}

	public static double calcularFatorial(int valor) {

		if (valor == 0) {
			return 1;

		}

		return valor * calcularFatorial(valor - 1);

	}

	public static double calcularInversoFatorial(int valor1, double valor2) {
		double calc = 0;
		for (int i = valor1; i > 0; i--) {
			calc += 1 / calcularFatorial(i);
		}
		return calc;

	}

	public static void impressao(double valor) {
		System.out.println(valor);

	}

}
