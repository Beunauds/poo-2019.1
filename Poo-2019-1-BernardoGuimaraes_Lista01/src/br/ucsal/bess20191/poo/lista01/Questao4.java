package br.ucsal.bess20191.poo.lista01;

import java.util.Scanner;

public class Questao4 {
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		String palavra;
		char letra;
		System.out.println("informe uma letra");
		palavra = sc.nextLine();
		palavra = palavra.toUpperCase();
		letra = palavra.charAt(0);
		if (letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U') {
			System.out.println("a letra � uma vogal");
		}
		if (letra == 'B' || letra == 'C' || letra == 'D' || letra == 'F' || letra == 'G' || letra == 'H' || letra == 'J'
				|| letra == 'K' || letra == 'L' || letra == 'M' || letra == 'N' || letra == 'P' || letra == 'Q'
				|| letra == 'R' || letra == 'S' || letra == 'T' || letra == 'V' || letra == 'W' || letra == 'X'
				|| letra == 'Y' || letra == 'z') {
			System.out.println("a letra � uma consoante");
		}

	}

}
