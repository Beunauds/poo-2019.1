package br.ucsal.bess20191.poo.lista01;

import java.util.Scanner;

public class Questao3 {
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int cont = 0;
		int num1;
		do {
			System.out.println("informe um valor de 0 a 100");
			num1 = sc.nextInt();
			if (num1 >= 0 && num1 <= 100) {
				cont = 1;
			} else {
				cont = 0;
			}
		} while (cont == 0);
		if (num1 >= 0 && num1 <= 49) {
			System.out.println("insuficiente");
		}
		if (num1 >= 50 && num1 <= 64) {
			System.out.println("regular");
		}
		if (num1 >= 65 && num1 <= 84) {
			System.out.println("bom");
		}
		if (num1 >= 85 && num1 <= 100) {
			System.out.println("otimo");
		}

	}
}
