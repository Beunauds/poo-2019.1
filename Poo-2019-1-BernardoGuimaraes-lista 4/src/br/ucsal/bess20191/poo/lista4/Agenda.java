package br.ucsal.bess20191.poo.lista4;

import java.util.ArrayList;
import java.util.List;

public class Agenda {

	private static List<Contato> contatos = new ArrayList<>();

	public static boolean ContemContato(String nome) {
		for (Contato contato : contatos) {
			if (contato.getNome().equals(nome)) {
				return true;
			}
		}
		return false;
	}

	public static void adcinarContato(Contato contato) {
		if (ContemContato(contato.getNome())) {
			System.out.println("Esse contato ja existe na lista");
		} else {
			contatos.add(contato);
		}
	}

	public static void removerContato(Contato contato) {
		if (ContemContato(contato.getNome())) {
			contatos.remove(contato);
		} else {
			System.out.println("Esse contato ja foi removido da lista");
		}
	}
	
	

}
