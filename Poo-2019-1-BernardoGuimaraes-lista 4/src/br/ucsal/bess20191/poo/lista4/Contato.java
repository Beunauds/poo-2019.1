package br.ucsal.bess20191.poo.lista4;

import java.util.Date;

public class Contato {

	private String nome;
	private String telefone;
	private Integer anoNacimento;
	private Date dataNacimento;
	private EnumPessoa tipo;

	public Contato(String nome, String telefone, Integer anoNacimento, Date dataNacimento, EnumPessoa tipo) {
		super();
		this.nome = nome;
		this.telefone = telefone;
		this.anoNacimento = anoNacimento;
		this.dataNacimento = dataNacimento;
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Integer getAnoNacimento() {
		return anoNacimento;
	}

	public void setAnoNacimento(Integer anoNacimento) {
		this.anoNacimento = anoNacimento;
	}

	public Date getDataNacimento() {
		return dataNacimento;
	}

	public void setDataNacimento(Date dataNacimento) {
		this.dataNacimento = dataNacimento;
	}

	public EnumPessoa getTipo() {
		return tipo;
	}

	public void setTipo(EnumPessoa tipo) {
		this.tipo = tipo;
	}

}
