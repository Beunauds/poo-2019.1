package br.ucsal.bess20191.poo.lista3;

import java.util.Scanner;

public class Atividade1 {
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int[] vetor = new int[5];
		obterNumeros(vetor);
		int maiorNumero = encontrarMaiorNumero(vetor);
		exibirMaiorNumero(maiorNumero);
	}

	public static void obterNumeros(int[] vetor) {
		System.out.println("informe 5 numeros inteiro");
		for (int i = 0; i < vetor.length; i++) {
			vetor[i] = sc.nextInt();
		}
		
	}

	public static int encontrarMaiorNumero(int[] vetor) {
		int valorMax = 0;
		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i] > valorMax) {
				valorMax = vetor[i];
			}
		}
		return valorMax;

	}

	public static void exibirMaiorNumero(int maiorNumero) {
		System.out.println("o maior numero �: " + maiorNumero);
	}

}
