import org.junit.Assert;
import org.junit.Test;

import br.ucsal.bess20191.poo.lista3.Atividade1;

public class Atividade1Teste {

	@Test

	public void Atividade1Test() {
		// Definišao de dados
		int[] vetor = new int[5];
		vetor[0] = 4;
		vetor[1] = 8;
		vetor[2] = 7;
		vetor[3] = 4;
		vetor[4] = 3;

		// metodo testado
		int maiorNumero = Atividade1.encontrarMaiorNumero(vetor);

		// resultado esperado
		int resultadoEsperado = 8;

		// comparašao do steste
		Assert.assertEquals(resultadoEsperado, maiorNumero);

	}

}
